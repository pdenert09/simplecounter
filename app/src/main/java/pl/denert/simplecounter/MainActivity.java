package pl.denert.simplecounter;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textViewCounter;
    Button buttonAdd;
    Button buttonReset;
    EditText editTextName;

    SharedPreferences sharedPreferences;
    private static final String sharedPreferencesName = "data";

    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // linkujemy elementy interfejsu
        textViewCounter = findViewById(R.id.textViewCounter);
        buttonAdd = findViewById(R.id.buttonAdd);
        buttonReset = findViewById(R.id.buttonReset);
        editTextName = findViewById(R.id.editTextName);

        // zwiększamy licznik o 1
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter++;
                textViewCounter.setText(counter+"");
            }
        });
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter = 0;
                textViewCounter.setText(counter+"");
            }
        });

        // inicjalizujemy SharedPreferences
        // https://developer.android.com/reference/android/content/SharedPreferences - oficjalna dokumentacja
        // https://developer.android.com/training/data-storage/shared-preferences - przykład użycia
        sharedPreferences = getSharedPreferences(sharedPreferencesName, 0);

    }

    // Dlaczego onResume i onPause?
    // https://developer.android.com/guide/components/activities/activity-lifecycle

    @Override
    public void onResume() {
        super.onResume();
        String name = sharedPreferences.getString("name", ""); // pobierz name, albo zwróć pusty obiekt typu String
        editTextName.setText(name);
        int mCounter = sharedPreferences.getInt("counter", 0); // pobierz wartość licznika, lub zwróc 0
        counter = mCounter;
        textViewCounter.setText(counter+"");
    }

    @Override
    public void onPause(){
        super.onPause();
        //zapisywanie danych z aplikacji
        sharedPreferences.edit().putInt("counter", counter).apply();
        sharedPreferences.edit().putString("name", editTextName.getText().toString()).apply();
    }
}
